#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "InputAction.h"
#include "BallInputConfigData.generated.h"

UCLASS()
class ARKANOIDCASTLE_API UBallInputConfigData : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly)
		UInputAction* LaunchBall;

	UPROPERTY(EditDefaultsOnly)
		UInputAction* RespawnBall;
};
