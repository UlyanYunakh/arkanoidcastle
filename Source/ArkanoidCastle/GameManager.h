#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BrickBase.h"
#include "GameManager.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAllBricksDestroyedSignature);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBeginDestroySignature, AActor*, Actor);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameStartedSignature);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameCompletedSignature);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnRoundStartedSignature, int, CurrentRoundNumber, class ABricksCollectionBase*, BricksCollection);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnRoundCompleteSignature, int, CurrentRoundNumber, int, NumberOfBricks, int, TotalBricksNumber);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnTimeAddedToRound, float, TimeAdded);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnManualRespawn, float, PenaltyTime);

UCLASS()
class ARKANOIDCASTLE_API AGameManager : public AActor
{
	GENERATED_BODY()
	
public:
	AGameManager();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

protected:
	UFUNCTION()
		void OnBrickDestroyed(AActor* Actor);

	UFUNCTION()
		void RespawnBricksCollection();

	UFUNCTION()
		void ClearRoundTimer();

	UFUNCTION()
		void EndCurrentRound();

public:

	// Game management

	UFUNCTION(BlueprintCallable)
		void StartNewRound();

	UFUNCTION(BlueprintCallable)
		void RestartRound();

	UFUNCTION(BlueprintCallable)
		void RestartGame();

	UFUNCTION(BlueprintCallable)
		void AddRoundTime(float TimeToAdd);

protected:
	UPROPERTY()
		int NumberOfBricks;

	UPROPERTY()
		int CurrentRoundNumber;

public:

	// Defaults

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class ABricksCollectionBase> BricksCollectionClass;

	UPROPERTY(EditDefaultsOnly)
		float RoundDuration;

	UPROPERTY(EditDefaultsOnly)
		float NumberOfRounds = 10;

	// Events

	UPROPERTY(BlueprintAssignable)
		FOnGameStartedSignature OnGameStarted;

	UPROPERTY(BlueprintAssignable)
		FOnGameCompletedSignature OnGameCompleted;

	UPROPERTY(BlueprintAssignable)
		FOnRoundStartedSignature OnRoundStarted;

	UPROPERTY(BlueprintAssignable)
		FOnRoundCompleteSignature OnRoundComplete;

	UPROPERTY(BlueprintAssignable)
		FOnTimeAddedToRound OnTimeAdded;

	UPROPERTY(BlueprintAssignable)
		FOnManualRespawn OnManualRespawn;

	// Other

	UPROPERTY(BlueprintReadOnly)
		FTimerHandle RoundTimerHandle;

	UPROPERTY(BlueprintReadOnly)
		class ABricksCollectionBase* BricksCollection;
};
