#include "BonusContainerComponent.h"
#include "PickableBase.h"
#include "HealthComponent.h"

UBonusContainerComponent::UBonusContainerComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UBonusContainerComponent::BeginPlay()
{
	Super::BeginPlay();

	if (UHealthComponent* HealthComp = Cast<UHealthComponent>(GetOwner()->GetComponentByClass(UHealthComponent::StaticClass())))
	{
		HealthComp->OnHealthDrained.AddDynamic(this, &UBonusContainerComponent::OnHealthDrained);
	}
}

void UBonusContainerComponent::OnHealthDrained(float Health, float DamageAmount, FVector HitLocation, AActor* DamageCauser)
{
	SpawnPickableBonus(GetOwner());
}

void UBonusContainerComponent::SpawnPickableBonus(AActor* OriginActor)
{
	GetWorld()->SpawnActor<APickableBase>(BonusPickableClass, OriginActor->GetActorLocation(), FRotator());
}

