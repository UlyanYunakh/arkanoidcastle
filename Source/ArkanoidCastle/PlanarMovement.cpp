// Fill out your copyright notice in the Description page of Project Settings.


#include "PlanarMovement.h"

UPlanarMovement::UPlanarMovement(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	SetPlaneConstraintEnabled(true);
	SetPlaneConstraintNormal(FVector::ZAxisVector);
}