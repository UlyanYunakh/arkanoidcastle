#include "LongPaddleBonus.h"
#include "PaddleBase.h"
#include "FieldBounds.h"
#include "Components/StaticMeshComponent.h"

ULongPaddleBonus::ULongPaddleBonus(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	auto tempStr = FString::Printf(TEXT("+ BIGGER RACKET for {0} seconds"));
	BonusDescription = FText::FromString(FString::Format(*tempStr, { FString::SanitizeFloat(BonusDuration) }));
}

void ULongPaddleBonus::ApplyBonus(APaddleBase* Target)
{
	Paddle = Target;
	UWorld* World = Paddle->GetWorld();

	if (Paddle->GetWorldTimerManager().IsTimerActive(Paddle->TimerHandle))
	{
		float Remaining = Paddle->GetWorldTimerManager().GetTimerRemaining(Paddle->TimerHandle);
		Paddle->GetWorldTimerManager().SetTimer(Paddle->TimerHandle, this, &ULongPaddleBonus::DeactivateBonus, BonusDuration + Remaining, false);
		return;
	}

	Paddle->SetPaddleMesh(LongPaddleMesh);

	FHitResult HitResults[] = { FHitResult(), FHitResult() };
	bool HitFound[] = { false, false };

	FVector Origin = Paddle->GetActorLocation();

	TArray<TEnumAsByte<EObjectTypeQuery>> TraceObjectTypes;
	TraceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldStatic));

	HitFound[0] = World->LineTraceSingleByObjectType(HitResults[0], Origin, Origin + FVector(0, 500, 0), TraceObjectTypes);
	HitFound[1] = World->LineTraceSingleByObjectType(HitResults[1], Origin, Origin - FVector(0, 500, 0), TraceObjectTypes);

	for (int i = 0; i < 2; i++)
	{
		if (HitFound[i])
		{
			float MeshHalfLenght = Paddle->PaddleMeshComp->Bounds.BoxExtent.Y;
			float Offset = MeshHalfLenght - FMath::Abs(HitResults[i].Distance);

			if (Offset <= 0) continue;

			float OffsetSign = i == 0 ? -1 : 1;
			Paddle->AddActorWorldOffset(FVector(0, Offset * OffsetSign, 0));
		}
	}

	Paddle->GetWorldTimerManager().SetTimer(Paddle->TimerHandle, this, &ULongPaddleBonus::DeactivateBonus, BonusDuration, false);
}

void ULongPaddleBonus::DeactivateBonus()
{
	if (Paddle)
	{
		Paddle->ResetPaddleMesh();
	}

	Super::DeactivateBonus();
}