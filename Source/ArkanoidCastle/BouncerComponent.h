#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BouncerComponent.generated.h"

USTRUCT()
struct FBounceResult
{
	GENERATED_BODY()

public:
	FBounceResult() {};

	FBounceResult(FVector bounceVector, bool frictionApplied)
	{
		BounceVector = bounceVector;
		FrictionApplied = frictionApplied;
	};

	FVector BounceVector;
	bool FrictionApplied;
};

UCLASS(BlueprintType, ClassGroup = (Arkanoid), meta = (BlueprintSpawnableComponent))
class ARKANOIDCASTLE_API UBouncerComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UBouncerComponent();
	
protected:
	virtual void BeginPlay() override;

public:
	UFUNCTION()
		virtual FBounceResult GetBounceResult(const FHitResult HitResult);
		
protected:
	UPROPERTY()
		class UFloatingPawnMovement* MovementComponent;
};
