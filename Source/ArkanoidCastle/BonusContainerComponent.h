#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BonusContainerComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ARKANOIDCASTLE_API UBonusContainerComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UBonusContainerComponent();

protected:
	virtual void BeginPlay() override;

protected:
	UFUNCTION()
		void SpawnPickableBonus(AActor* OriginActor);

	UFUNCTION()
		void OnHealthDrained(float Health, float DamageAmount, FVector HitLocation, AActor* DamageCauser);
		
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class APickableBase> BonusPickableClass;
};
