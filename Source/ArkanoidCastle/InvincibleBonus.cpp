#include "InvincibleBonus.h"
#include "PaddleBase.h"
#include "GameManager.h"
#include "BallLauncherComponent.h"
#include "BallBase.h"
#include "BouncingMovementComponent.h"
#include "IgnoringBounceHandler.h"
#include <Kismet/GameplayStatics.h>

UInvincibleBonus::UInvincibleBonus(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	auto tempStr = FString::Printf(TEXT("+ INSTOPABLE for {0} seconds"));
	BonusDescription = FText::FromString(FString::Format(*tempStr, { FString::SanitizeFloat(BonusDuration) }));
}

void UInvincibleBonus::ApplyBonus(APaddleBase* Paddle)
{
	Ball = Paddle->BallLauncherComp->GetBall();

	if (Ball)
	{
		if (Ball->GetWorldTimerManager().IsTimerActive(Ball->TimerHandle))
		{
			float Remaining = Ball->GetWorldTimerManager().GetTimerRemaining(Ball->TimerHandle);
			Ball->GetWorldTimerManager().SetTimer(Ball->TimerHandle, this, &UInvincibleBonus::DeactivateBonus, BonusDuration + Remaining, false);
			return;
		}

		Ball->SetBallMaterial(InvincibleMaterial);
		Ball->BouncingMovementComp->SetNewBounceHandler(UIgnoringBounceHandler::StaticClass());

		Ball->GetWorldTimerManager().SetTimer(Ball->TimerHandle, this, &UInvincibleBonus::DeactivateBonus, BonusDuration, false);
	}
}

void UInvincibleBonus::DeactivateBonus()
{
	if (Ball)
	{
		Ball->ResetBallMaterial();
		Ball->BouncingMovementComp->ResetBounceHandler();
	}

	Super::DeactivateBonus();
}