#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickableBase.generated.h"

UCLASS()
class ARKANOIDCASTLE_API APickableBase : public AActor
{
	GENERATED_BODY()
	
public:
	APickableBase();

protected:
	virtual void BeginPlay() override;

protected:
	UFUNCTION()
		void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void DestroyPickable();

protected:
	UPROPERTY()
		class USimpleMovementComponent* Movement;

	UPROPERTY(EditDefaultsOnly)
		class UBoxComponent* Box;

public:
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class UBonus> BonusClass;

};
