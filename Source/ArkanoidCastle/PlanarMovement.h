// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "PlanarMovement.generated.h"

UCLASS(BlueprintType, ClassGroup = (Arkanoid), meta = (BlueprintSpawnableComponent))
class ARKANOIDCASTLE_API UPlanarMovement : public UFloatingPawnMovement
{
	GENERATED_BODY()
	
public:
	UPlanarMovement(const FObjectInitializer& ObjectInitializer);
};
