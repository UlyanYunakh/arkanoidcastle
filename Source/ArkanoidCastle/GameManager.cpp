#include "GameManager.h"
#include "BrickBase.h"
#include "BricksCollectionBase.h"
#include "Components/ChildActorComponent.h"
#include <Kismet/GameplayStatics.h>

AGameManager::AGameManager()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AGameManager::BeginPlay()
{
	Super::BeginPlay();

	if (ABricksCollectionBase* OnLevelBrickCollection = Cast<ABricksCollectionBase>(UGameplayStatics::GetActorOfClass(GetWorld(), ABricksCollectionBase::StaticClass())))
	{
		BricksCollection = OnLevelBrickCollection;
	}

	RestartGame();
}

void AGameManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!GetWorldTimerManager().IsTimerActive(RoundTimerHandle) && CurrentRoundNumber <= NumberOfRounds)
	{
		EndCurrentRound();
	}
}

void AGameManager::StartNewRound()
{
	ClearRoundTimer();

	++CurrentRoundNumber;
	if (CurrentRoundNumber > NumberOfRounds)
	{
		OnGameCompleted.Broadcast();
		return;
	}

	RestartRound();
}

void AGameManager::RestartRound()
{
	ClearRoundTimer();
	RespawnBricksCollection();
	OnRoundStarted.Broadcast(CurrentRoundNumber, BricksCollection);
	GetWorldTimerManager().SetTimer(RoundTimerHandle, this, &AGameManager::ClearRoundTimer, RoundDuration, false);
}

void AGameManager::EndCurrentRound()
{
	ClearRoundTimer();
	OnRoundComplete.Broadcast(CurrentRoundNumber, NumberOfBricks, BricksCollection->TotalBricksNumber);
	StartNewRound();
}

void AGameManager::RestartGame()
{
	ClearRoundTimer();
	CurrentRoundNumber = 0;
	OnGameStarted.Broadcast();
	StartNewRound();
}

void AGameManager::AddRoundTime(float TimeToAdd)
{
	float RemainingTime = GetWorldTimerManager().GetTimerRemaining(RoundTimerHandle);
	GetWorldTimerManager().SetTimer(RoundTimerHandle, this, &AGameManager::ClearRoundTimer, RemainingTime + TimeToAdd, false);
	OnTimeAdded.Broadcast(TimeToAdd);
}

void AGameManager::ClearRoundTimer()
{
	GetWorldTimerManager().ClearTimer(RoundTimerHandle);
}

void AGameManager::OnBrickDestroyed(AActor* Actor)
{
	--NumberOfBricks;

	if (NumberOfBricks <= 0)
	{
		ClearRoundTimer();
	}
}

void AGameManager::RespawnBricksCollection()
{
	if (BricksCollection)
	{
		FTransform Origin = BricksCollection->GetActorTransform();
		if (ABricksCollectionBase* NewBricksCollection = GetWorld()->SpawnActor<ABricksCollectionBase>(BricksCollectionClass, Origin))
		{
			BricksCollection->Destroy();
			BricksCollection = NewBricksCollection;

			NumberOfBricks = BricksCollection->Bricks.Num();

			for (ABrickBase* Brick : BricksCollection->Bricks)
			{
				Brick->OnDestroyed.AddDynamic(this, &AGameManager::OnBrickDestroyed);
			}
		}
	}
}