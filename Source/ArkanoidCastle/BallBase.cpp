#include "BallBase.h"
#include "BouncingMovementComponent.h"
#include "Components/StaticMeshComponent.h"
#include "DamageComponent.h"

ABallBase::ABallBase()
{
	PrimaryActorTick.bCanEverTick = true;

	BallMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BallMesh"));
	SetRootComponent(BallMeshComp);

	BouncingMovementComp = CreateDefaultSubobject<UBouncingMovementComponent>(TEXT("BouncingMovementComponent"));

	DamageComp = CreateDefaultSubobject<UDamageComponent>(TEXT("DamageComponent"));
}

void ABallBase::BeginPlay()
{
	Super::BeginPlay();

	DefaultBallMaterial = BallMeshComp->GetMaterial(0)->GetMaterial();
}

void ABallBase::StartMovement()
{
	BouncingMovementComp->Speed = BallSpeed;
}

void ABallBase::SetBallMaterial(UMaterial* Material)
{
	BallMeshComp->SetMaterial(0, Material);
}

void ABallBase::ResetBallMaterial()
{
	SetBallMaterial(DefaultBallMaterial);
}