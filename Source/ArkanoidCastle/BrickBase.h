#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BrickBase.generated.h"

UCLASS()
class ARKANOIDCASTLE_API ABrickBase : public AActor
{
	GENERATED_BODY()
	
public:
	ABrickBase();
};
