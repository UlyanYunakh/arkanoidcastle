#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PaddleBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBonusAccept, class UBonus*, Bonus);

UCLASS()
class ARKANOIDCASTLE_API APaddleBase : public APawn
{
	GENERATED_BODY()

public:
	APaddleBase();

protected:
	virtual void BeginPlay() override;

public:
	UFUNCTION()
		void AcceptBonus(class UBonus* Bonus);

	UFUNCTION()
		void SetPaddleMesh(class UStaticMesh* NewMesh);

	UFUNCTION()
		void ResetPaddleMesh();

protected:
	UPROPERTY()
		class UStaticMesh* DefaultPaddleMesh;

public:
	// Defaults

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class UStaticMeshComponent* PaddleMeshComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class UBallLauncherComponent* BallLauncherComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class UBouncerComponent* BouncerComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class UControlComponent* ControlComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class USimpleMovementComponent* MovementComp;

public:
	UPROPERTY(BlueprintReadOnly)
		FTimerHandle TimerHandle;

	UPROPERTY(BlueprintAssignable, Category = "Events")
		FOnBonusAccept OnBonusAccept;
};
