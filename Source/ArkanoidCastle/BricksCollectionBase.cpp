#include "BricksCollectionBase.h"
#include "Components/ChildActorComponent.h"
#include "Components/SceneComponent.h"

ABricksCollectionBase::ABricksCollectionBase()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ABricksCollectionBase::BeginPlay()
{
	Super::BeginPlay();

	TArray<USceneComponent*> ChildrenCompArr;
	GetRootComponent()->GetChildrenComponents(true, ChildrenCompArr);
	for (USceneComponent* ChildComp : ChildrenCompArr)
	{
		if (UChildActorComponent* ChildActorComp = Cast<UChildActorComponent>(ChildComp))
		{
			if (ABrickBase* Brick = Cast<ABrickBase>(ChildActorComp->GetChildActor()))
			{
				Bricks.Add(Brick);
			}
		}
	}

	TotalBricksNumber = Bricks.Num();
}
