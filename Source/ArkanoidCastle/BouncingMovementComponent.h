#pragma once

#include "CoreMinimal.h"
#include "SimpleMovementComponent.h"
#include "BouncingMovementComponent.generated.h"

UCLASS(BlueprintType, ClassGroup = (Arkanoid), meta = (BlueprintSpawnableComponent))
class ARKANOIDCASTLE_API UBouncingMovementComponent : public USimpleMovementComponent
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;

protected:
	UFUNCTION()
	virtual void BounceOnHit(const FHitResult HitResult);

public:
	UFUNCTION()
		void SetNewBounceHandler(TSubclassOf<class UBounceHandler> BounceHandlerClass);

	UFUNCTION()
		void ResetBounceHandler();

protected:
	UPROPERTY()
		class UBounceHandler* DefaulsBounceHandler;
};
