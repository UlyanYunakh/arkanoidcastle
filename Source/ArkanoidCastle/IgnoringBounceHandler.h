#pragma once

#include "CoreMinimal.h"
#include "BounceHandler.h"
#include "IgnoringBounceHandler.generated.h"

UCLASS()
class ARKANOIDCASTLE_API UIgnoringBounceHandler : public UBounceHandler
{
	GENERATED_BODY()
	
public:
	virtual FVector GetBounceVector(FVector ForwardVector, const FHitResult HitResult) override;
};
