#include "PaddleBase.h"
#include "BallLauncherComponent.h"
#include "Bonus.h"
#include "BouncerComponent.h"
#include "Components/StaticMeshComponent.h"
#include "ControlComponent.h"
#include "SimpleMovementComponent.h"

APaddleBase::APaddleBase()
{
	PrimaryActorTick.bCanEverTick = true;

	PaddleMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PaddleMesh"));
	SetRootComponent(PaddleMeshComp);

	BallLauncherComp = CreateDefaultSubobject<UBallLauncherComponent>(TEXT("BallLauncherComponent"));

	BouncerComp = CreateDefaultSubobject<UBouncerComponent>(TEXT("BouncerComponent"));
	
	ControlComp = CreateDefaultSubobject<UControlComponent>(TEXT("ControlComponent"));
	
	MovementComp = CreateDefaultSubobject<USimpleMovementComponent>(TEXT("MovementComponent"));
}

void APaddleBase::BeginPlay()
{
	Super::BeginPlay();

	DefaultPaddleMesh = PaddleMeshComp->GetStaticMesh();
	MovementComp->ForwardVector = FVector(0, 0, 0);
}

void APaddleBase::AcceptBonus(UBonus* Bonus)
{
	OnBonusAccept.Broadcast(Bonus);
	Bonus->ApplyBonus(this);
}

void APaddleBase::SetPaddleMesh(UStaticMesh* NewMesh)
{
	PaddleMeshComp->SetStaticMesh(NewMesh);
}

void APaddleBase::ResetPaddleMesh()
{
	SetPaddleMesh(DefaultPaddleMesh);
}