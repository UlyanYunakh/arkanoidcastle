#include "ControlComponent.h"
#include "InputConfigData.h"
#include "SimpleMovementComponent.h"
#include <EnhancedInputSubsystems.h>
#include <EnhancedInputComponent.h>

UControlComponent::UControlComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UControlComponent::BeginPlay()
{
	Super::BeginPlay();

	if (APawn* Owner = Cast<APawn>(GetOwner()))
	{
		MovementComponent = Owner->FindComponentByClass<USimpleMovementComponent>();

		if (APlayerController* PlayerController = Cast<APlayerController>(Owner->GetController()))
		{
			UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer());
			UEnhancedInputComponent* InputComponent = Cast<UEnhancedInputComponent>(Owner->InputComponent);
			if (Subsystem && InputComponent && InputMapping)
			{
				Subsystem->AddMappingContext(InputMapping, 0);
				BindControls(InputComponent);
			}
		}
	}
}

void UControlComponent::BindControls(UEnhancedInputComponent* InputComponent)
{
	if (InputActions)
	{
		InputComponent->BindAction(InputActions->InputMove, ETriggerEvent::Triggered, this, &UControlComponent::MoveEvent);

		InputComponent->BindAction(InputActions->InputMove, ETriggerEvent::Completed, this, &UControlComponent::StopMoveEvent);
		InputComponent->BindAction(InputActions->InputMove, ETriggerEvent::Canceled, this, &UControlComponent::StopMoveEvent);
	}
}

void UControlComponent::MoveEvent(const FInputActionValue& Value)
{
	if (MovementComponent)
	{
		MovementComponent->ForwardVector = FVector(0, Value.Get<float>(), 0);
	}
}

void UControlComponent::StopMoveEvent(const FInputActionValue& Value)
{
	if (MovementComponent)
	{
		MovementComponent->ForwardVector = FVector(0, 0, 0);
	}
}