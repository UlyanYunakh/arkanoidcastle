#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DamageComponent.generated.h"


UCLASS(BlueprintType, ClassGroup = (Arkanoid), meta = (BlueprintSpawnableComponent))
class ARKANOIDCASTLE_API UDamageComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UDamageComponent();

protected:
	virtual void BeginPlay() override;
	
private:
	UFUNCTION()
		void OnActorHit(class AActor* SelfActor, class AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);

public:
	UPROPERTY(EditDefaultsOnly)
		float Damage;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<UDamageType> DamageType;
};
