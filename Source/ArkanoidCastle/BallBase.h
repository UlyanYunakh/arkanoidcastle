#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BallBase.generated.h"

UCLASS()
class ARKANOIDCASTLE_API ABallBase : public AActor
{
	GENERATED_BODY()
	
public:
	ABallBase();

protected:
	virtual void BeginPlay() override;

public:
	UFUNCTION()
		void StartMovement();

	UFUNCTION()
		void SetBallMaterial(class UMaterial* Material);

	UFUNCTION()
		void ResetBallMaterial();

protected:
	UPROPERTY()
		class UMaterial* DefaultBallMaterial;

public:
	// Defaults

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class UStaticMeshComponent* BallMeshComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class UBouncingMovementComponent* BouncingMovementComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class UDamageComponent* DamageComp;

	UPROPERTY(EditDefaultsOnly)
		float BallSpeed = 1000.0f;

public:
	UPROPERTY(BlueprintReadOnly)
		FTimerHandle TimerHandle;

};
