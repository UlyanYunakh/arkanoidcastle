#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ControlComponent.generated.h"


UCLASS(BlueprintType, ClassGroup = (Arkanoid), meta = (BlueprintSpawnableComponent))
class ARKANOIDCASTLE_API UControlComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UControlComponent();

	virtual void BeginPlay() override;

private:
	UFUNCTION()
		void BindControls(class UEnhancedInputComponent* InputComponent);

	UFUNCTION()
		void MoveEvent(const FInputActionValue& Value);

	UFUNCTION()
		void StopMoveEvent(const FInputActionValue& Value);

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		class UInputMappingContext* InputMapping;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		class UInputConfigData* InputActions;

private:
	UPROPERTY()
		class USimpleMovementComponent* MovementComponent;
};
