#include "HealthComponent.h"
#include <Kismet/GameplayStatics.h>

UHealthComponent::UHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	MaxHealth = 100.0f;
}

void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	if (AActor* Owner = GetOwner())
	{
		Owner->OnTakePointDamage.AddDynamic(this, &UHealthComponent::HandleTakeDamage);
	}

	Health = MaxHealth;
}

void UHealthComponent::HandleTakeDamage(AActor* DamagedActor, float Damage, class AController* InstigatedBy, FVector HitLocation, class UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const class UDamageType* DamageType, AActor* DamageCauser)
{
	if (Damage <= 0 || Health <= 0)
	{
		return;
	}

	Health = FMath::Clamp(Health - Damage, 0.0f, MaxHealth);
	if (Health <= 0)
	{
		OnHealthDrained.Broadcast(Health, Damage, HitLocation, DamageCauser);
	}
	else
	{
		OnHealthChanged.Broadcast(Health, Damage, HitLocation, DamageCauser);
	}
}
