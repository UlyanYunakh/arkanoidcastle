#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SimpleMovementComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHitSignature, FHitResult, HitResult);

UCLASS(BlueprintType, ClassGroup = (Arkanoid), meta = (BlueprintSpawnableComponent))
class ARKANOIDCASTLE_API USimpleMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	USimpleMovementComponent();

protected:
	virtual void BeginPlay() override;

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UPROPERTY(BlueprintAssignable, Category = "Events")
		FOnHitSignature OnHit;

protected:
	UPROPERTY()
		class AActor* Owner;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Speed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector ForwardVector;
};
