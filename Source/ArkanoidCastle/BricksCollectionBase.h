#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BrickBase.h"
#include "BricksCollectionBase.generated.h"

UCLASS()
class ARKANOIDCASTLE_API ABricksCollectionBase : public AActor
{
	GENERATED_BODY()
	
public:	
	ABricksCollectionBase();
	
protected:
	virtual void BeginPlay() override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<ABrickBase*> Bricks;

	UPROPERTY(BlueprintReadOnly)
		int TotalBricksNumber;
};
