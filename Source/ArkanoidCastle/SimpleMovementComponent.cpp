#include "SimpleMovementComponent.h"

USimpleMovementComponent::USimpleMovementComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void USimpleMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	Owner = GetOwner();
	ForwardVector = Owner->GetActorForwardVector();
}

void USimpleMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (Owner)
	{
		FHitResult HitResult;
		Owner->AddActorWorldOffset(ForwardVector * Speed * DeltaTime, true, &HitResult);

		if (HitResult.IsValidBlockingHit())
		{
			OnHit.Broadcast(HitResult);
		}
	}
}
