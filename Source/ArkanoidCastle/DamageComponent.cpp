#include "DamageComponent.h"
#include <Kismet/GameplayStatics.h>

UDamageComponent::UDamageComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UDamageComponent::BeginPlay()
{
	Super::BeginPlay();

	AActor* Owner = GetOwner();

	if (Owner)
	{
		Owner->OnActorHit.AddDynamic(this, &UDamageComponent::OnActorHit);
	}
}
	
void UDamageComponent::OnActorHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	UGameplayStatics::ApplyPointDamage(OtherActor, Damage, Hit.ImpactNormal, Hit, GetOwner()->GetInstigatorController(), SelfActor, DamageType);
}

