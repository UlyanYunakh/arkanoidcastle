#include "PickableBase.h"
#include "SimpleMovementComponent.h"
#include "Components/BoxComponent.h"
#include "Bonus.h"
#include "PaddleBase.h"

APickableBase::APickableBase()
{
	PrimaryActorTick.bCanEverTick = true;

	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Root"));
	SetRootComponent(Box);

	Movement = CreateDefaultSubobject<USimpleMovementComponent>(TEXT("Movement Comp"));
}

void APickableBase::BeginPlay()
{
	Super::BeginPlay();
	
	Movement->ForwardVector = -FVector::XAxisVector;
	Movement->Speed = 100;

	Box->OnComponentBeginOverlap.AddDynamic(this, &APickableBase::OnOverlap);
}

void APickableBase::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (APaddleBase* Paddle = Cast<APaddleBase>(OtherActor))
	{
		TArray<USceneComponent*> ChildrenComps;
		Box->GetChildrenComponents(true, ChildrenComps);
		for (USceneComponent* ChildComp : ChildrenComps)
		{
			ChildComp->DestroyComponent();
		}
		Box->DestroyComponent();
		Movement->DestroyComponent();

		UBonus* BonusObj = NewObject<UBonus>(this, BonusClass);
		BonusObj->OnBonusEffectEnded.AddDynamic(this, &APickableBase::DestroyPickable);
		Paddle->AcceptBonus(BonusObj);
	}
}

void APickableBase::DestroyPickable()
{
	Destroy();
}