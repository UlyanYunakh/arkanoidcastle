#include "BounceHandler.h"

FVector UBounceHandler::GetBounceVector(FVector ForwardVector, const FHitResult HitResult)
{
	return ForwardVector.MirrorByVector(HitResult.Normal);
}