#include "IgnoringBounceHandler.h"
#include "BrickBase.h"

FVector UIgnoringBounceHandler::GetBounceVector(FVector ForwardVector, const FHitResult HitResult)
{
	if (ABrickBase* Brick = Cast<ABrickBase>(HitResult.GetActor()))
	{
		return ForwardVector;
	}

	return ForwardVector.MirrorByVector(HitResult.Normal);
}