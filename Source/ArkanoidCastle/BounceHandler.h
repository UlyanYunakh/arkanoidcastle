#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "BounceHandler.generated.h"

UCLASS()
class ARKANOIDCASTLE_API UBounceHandler : public UObject
{
	GENERATED_BODY()
	
public:
	UFUNCTION()
		virtual FVector GetBounceVector(FVector ForwardVector, const FHitResult HitResult);
};
