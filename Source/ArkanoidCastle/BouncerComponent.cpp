#include "BouncerComponent.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/FloatingPawnMovement.h"

UBouncerComponent::UBouncerComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UBouncerComponent::BeginPlay()
{
	if (AActor* Owner = GetOwner())
	{
		MovementComponent = Owner->FindComponentByClass<UFloatingPawnMovement>();
	}
}

FBounceResult UBouncerComponent::GetBounceResult(const FHitResult HitResult)
{
	FVector bounceVector;
	bool friction = false;

	if (AActor* Owner = GetOwner())
	{
		bounceVector = HitResult.ImpactPoint - Owner->GetActorLocation();
		bounceVector.Z = 0;
		bounceVector.Normalize();
		bounceVector = bounceVector + HitResult.Normal;

		/* 
		FVector currentVelocity(0, 0, 0);
		if (MovementComponent)
		{
			currentVelocity = MovementComponent->Velocity;
			currentVelocity.Normalize();
		}

		if (!currentVelocity.IsZero())
		{
			friction = true;
			if (FVector::DotProduct(currentVelocity, bounceVector) < 0)
			{
				bounceVector = bounceVector + currentVelocity;
			}
			else
			{
				bounceVector = bounceVector - currentVelocity;
			}
		}
		*/
	}

	bounceVector.Z = 0;
	bounceVector.Normalize();
	return FBounceResult(bounceVector, friction);
}

