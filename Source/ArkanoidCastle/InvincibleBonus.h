#pragma once

#include "CoreMinimal.h"
#include "Bonus.h"
#include "InvincibleBonus.generated.h"

UCLASS(Blueprintable)
class ARKANOIDCASTLE_API UInvincibleBonus : public UBonus
{
	GENERATED_BODY()

public:
	UInvincibleBonus(const FObjectInitializer& ObjectInitializer);

public:
	virtual void ApplyBonus(class APaddleBase* Paddle) override;

	virtual void DeactivateBonus() override;

protected:
	UPROPERTY()
		class ABallBase* Ball;

	UPROPERTY()
		class UBouncingMovementComponent* BouncingComp;

public:
	UPROPERTY(EditDefaultsOnly)
		class UMaterial* InvincibleMaterial;

	UPROPERTY(EditDefaultsOnly)
		float BonusDuration = 2.0f;
};
