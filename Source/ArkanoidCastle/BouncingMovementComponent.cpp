#include "BouncingMovementComponent.h"
#include "BouncerComponent.h"
#include "BounceHandler.h"

void UBouncingMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	ResetBounceHandler();

	OnHit.AddDynamic(this, &UBouncingMovementComponent::BounceOnHit);
}

void UBouncingMovementComponent::BounceOnHit(const FHitResult HitResult)
{
	FVector NewForwardVector(0,0,0);

	if (AActor* HitActor = HitResult.GetActor())
	{
		if (UBouncerComponent* Bouncer = HitActor->FindComponentByClass<UBouncerComponent>())
		{
			FBounceResult BounceResult = Bouncer->GetBounceResult(HitResult);
			NewForwardVector = BounceResult.BounceVector;
		}
	}

	if (NewForwardVector.IsZero())
	{
		NewForwardVector = DefaulsBounceHandler->GetBounceVector(ForwardVector, HitResult);
	}

	NewForwardVector.Z = ForwardVector.Z;
	ForwardVector = NewForwardVector;
}

void UBouncingMovementComponent::SetNewBounceHandler(TSubclassOf<class UBounceHandler> BounceHandlerClass)
{
	DefaulsBounceHandler = NewObject<UBounceHandler>(this, BounceHandlerClass);
}

void UBouncingMovementComponent::ResetBounceHandler()
{
	SetNewBounceHandler(UBounceHandler::StaticClass());
}
