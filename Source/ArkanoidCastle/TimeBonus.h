#pragma once

#include "CoreMinimal.h"
#include "Bonus.h"
#include "TimeBonus.generated.h"

UCLASS(Blueprintable)
class ARKANOIDCASTLE_API UTimeBonus : public UBonus
{
	GENERATED_BODY()

public:
	UTimeBonus(const FObjectInitializer& ObjectInitializer);

public:
	virtual void ApplyBonus(class APaddleBase* Paddle) override;

public:
	UPROPERTY(EditDefaultsOnly)
		float TimeToAdd = 15.0f;
};
