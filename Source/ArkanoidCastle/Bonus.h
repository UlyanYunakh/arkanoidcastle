#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Bonus.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnBonusEffectEndedSignature);

UCLASS(Blueprintable)
class ARKANOIDCASTLE_API UBonus : public UObject
{
	GENERATED_BODY()
	
public:
	UFUNCTION()
		virtual void ApplyBonus(class APaddleBase* Paddle);

	UFUNCTION()
		virtual void DeactivateBonus();

public:
	UPROPERTY(BlueprintAssignable)
		FOnBonusEffectEndedSignature OnBonusEffectEnded;

public:
	UPROPERTY(BlueprintReadOnly)
		FText BonusDescription;
};
