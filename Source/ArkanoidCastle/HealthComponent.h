#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnHealthChangedSignature, float, Health, float, DamageAmount, FVector, HitLocation, AActor*, DamageCauser);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnHealthDrained, float, Health, float, DamageAmount, FVector, HitLocation, AActor*, DamageCauser);

UCLASS(BlueprintType, ClassGroup = (Arkanoid), meta = (BlueprintSpawnableComponent))
class ARKANOIDCASTLE_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UHealthComponent();

protected:
	virtual void BeginPlay() override;

private:
	UFUNCTION()
		void HandleTakeDamage(AActor* DamagedActor, float Damage, class AController* InstigatedBy, FVector HitLocation, class UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const class UDamageType* DamageType, AActor* DamageCauser);

public:
	UPROPERTY(BlueprintAssignable, Category = "Events")
		FOnHealthChangedSignature OnHealthChanged;

	UPROPERTY(BlueprintAssignable, Category = "Events")
		FOnHealthDrained OnHealthDrained;

	UPROPERTY(EditDefaultsOnly)
		float MaxHealth;

protected:
	UPROPERTY()
		float Health;
};
