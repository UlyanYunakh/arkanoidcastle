#include "BallLauncherComponent.h"
#include "Kismet/GameplayStatics.h"
#include "BallInputConfigData.h"
#include "BallBase.h"
#include "GameManager.h"
#include <EnhancedInputSubsystems.h>
#include <EnhancedInputComponent.h>

UBallLauncherComponent::UBallLauncherComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UBallLauncherComponent::BeginPlay()
{
	Super::BeginPlay();

	if (APawn* Owner = Cast<APawn>(GetOwner()))
	{
		RespawnBall();

		if (APlayerController* PlayerController = Cast<APlayerController>(Owner->GetController()))
		{
			UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer());
			UEnhancedInputComponent* InputComponent = Cast<UEnhancedInputComponent>(Owner->InputComponent);
			if (Subsystem && InputMapping)
			{
				Subsystem->AddMappingContext(InputMapping, 0);
			}
			if (InputComponent && InputActions)
			{
				InputComponent->BindAction(InputActions->LaunchBall, ETriggerEvent::Triggered, this, &UBallLauncherComponent::LauchBall);
				InputComponent->BindAction(InputActions->RespawnBall, ETriggerEvent::Completed, this, &UBallLauncherComponent::RespawnBallByPlayer);
			}
		}
	}
}

void UBallLauncherComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UBallLauncherComponent::LauchBall()
{
	if (Ball && Ball->IsAttachedTo(GetOwner()))
	{
		Ball->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		Ball->StartMovement();
	}
}

void UBallLauncherComponent::RespawnBallByPlayer()
{
	RespawnBall();

	if (AGameManager* GM = Cast<AGameManager>(UGameplayStatics::GetActorOfClass(GetOwner()->GetWorld(), AGameManager::StaticClass())))
	{
		GM->AddRoundTime(BallManualRespawnTimePenalty);
		GM->OnManualRespawn.Broadcast(BallManualRespawnTimePenalty);
	}
}

void UBallLauncherComponent::RespawnBall()
{
	if (Ball)
	{
		Ball->OnDestroyed.RemoveDynamic(this, &UBallLauncherComponent::OnBallDestroyed);
		Ball->Destroy();
	}

	FTransform Origin = GetRelativeTransform();
	Ball = GetOwner()->GetWorld()->SpawnActor<ABallBase>(BallClass, Origin);
	Ball->AttachToActor(GetOwner(), FAttachmentTransformRules::KeepRelativeTransform);
	Ball->OnDestroyed.AddDynamic(this, &UBallLauncherComponent::OnBallDestroyed);
}

void UBallLauncherComponent::OnBallDestroyed(AActor* Actor)
{
	RespawnBall();

	if (AGameManager* GM = Cast<AGameManager>(UGameplayStatics::GetActorOfClass(GetOwner()->GetWorld(), AGameManager::StaticClass())))
	{
		GM->AddRoundTime(BallRespawnTimePenalty);
		GM->OnManualRespawn.Broadcast(BallRespawnTimePenalty);
	}
}

ABallBase* UBallLauncherComponent::GetBall()
{
	return Ball;
}
