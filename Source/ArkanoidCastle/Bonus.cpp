#include "Bonus.h"
#include "PaddleBase.h"

void UBonus::ApplyBonus(APaddleBase* Paddle)
{
	DeactivateBonus();
}

void UBonus::DeactivateBonus()
{
	OnBonusEffectEnded.Broadcast();
}