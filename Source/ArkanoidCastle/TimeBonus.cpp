#include "TimeBonus.h"
#include "PaddleBase.h"
#include "GameManager.h"
#include <Kismet/GameplayStatics.h>

UTimeBonus::UTimeBonus(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	auto tempStr = FString::Printf(TEXT("+ {0} seconds"));
	BonusDescription = FText::FromString(FString::Format(*tempStr, { FString::SanitizeFloat(TimeToAdd) }));
}

void UTimeBonus::ApplyBonus(APaddleBase* Paddle)
{
	if (AGameManager* GM = Cast<AGameManager>(UGameplayStatics::GetActorOfClass(Paddle->GetWorld(), AGameManager::StaticClass())))
	{
		GM->AddRoundTime(TimeToAdd);
	}

	DeactivateBonus();
}
