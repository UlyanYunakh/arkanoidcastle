#pragma once

#include "CoreMinimal.h"
#include "Bonus.h"
#include "LongPaddleBonus.generated.h"

UCLASS()
class ARKANOIDCASTLE_API ULongPaddleBonus : public UBonus
{
	GENERATED_BODY()

public:
	ULongPaddleBonus(const FObjectInitializer& ObjectInitializer);

public:
	virtual void ApplyBonus(class APaddleBase* Target) override;

	virtual void DeactivateBonus() override;

protected:
	UPROPERTY()
		class APaddleBase* Paddle;

public:
	UPROPERTY(EditDefaultsOnly)
		class UStaticMesh* LongPaddleMesh;

	UPROPERTY(EditDefaultsOnly)
		float BonusDuration = 5.0f;

};
