#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "BallLauncherComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ARKANOIDCASTLE_API UBallLauncherComponent : public USceneComponent
{
	GENERATED_BODY()

public:
	UBallLauncherComponent();

protected:
	virtual void BeginPlay() override;

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	UFUNCTION()
		void LauchBall();

	UFUNCTION()
		void RespawnBallByPlayer();

	UFUNCTION()
		void OnBallDestroyed(class AActor* Actor);

public:
	UFUNCTION()
		class ABallBase* GetBall();

	UFUNCTION(BlueprintCallable)
		void RespawnBall();

protected:
	UPROPERTY()
		class ABallBase* Ball;

public:

	// Defaults

	UPROPERTY(EditDefaultsOnly)
		float BallRespawnTimePenalty = - 1.0f;


	UPROPERTY(EditDefaultsOnly)
		float BallManualRespawnTimePenalty = -3.0f;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class ABallBase> BallClass;

	UPROPERTY(EditDefaultsOnly)
		class UInputMappingContext* InputMapping;

	UPROPERTY(EditDefaultsOnly)
		class UBallInputConfigData* InputActions;
		
};
